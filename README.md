# Tiny Fake API

This is a small/tiny project. It represents an NodeJS express server to exposes fake data.



# Credits

Repo icon by [Freepik](https://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com/), licensed by  [Creative Commons BY 3.0](http://creativecommons.org/licenses/by/3.0/).