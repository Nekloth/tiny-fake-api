const express = require('express')
const app = express()
const port = 3000
const ContentGenerator = require("randomcontentgenerator").ContentGenerator;



let schema = {
	"fields": {
		"CreatureName": [
			"@g{0} Dragon",
			"@g{0} Troll"
		],
		"Attack": {
			"min": 5,
			"max": 11
		},
		"Health": {
			"min": 100,
			"max": 500
		},
		"Description": {
			"options": [
				[
					"Increased @{0}% move speed while hidden. ",
					"Deal bonus @{1} @g{0} damage while attacking. "
				],
				[
					"While moving has increased resistance.",
					"Immune to @g{0} damage."
				]
			],
			"properties": [
				{
					"min": 5,
					"max": 11
				},
				{
					"min": 100,
					"max": 251
				}
			]
		}
	},
	"globalProperties": [
		[
			"Fire",
			"Frost",
			"Earth"
		]
	]
}

let generator = new ContentGenerator(schema);


app.get('/', (req, res) => res.send(generator.build()))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))